<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Lunch...shall we?</title>
    <link rel="stylesheet" href="assets/css/styles.css">
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="assets/js/jquery-1.11.2.min.js"></script>
    <script src="assets/js/rotator.js"></script>
</head>

<body>
<div id="container">

    <?php
    include('Lunch.php');

    $lunch = new Lunch;
    $form = $lunch->getForm();

    if(isset($_POST['submit'])){
        $chosen = array();
        foreach($_POST as $choice=>$val){
            if($val!='submit'){
                array_push($chosen, $val);
            }
        }
        echo '<h1>Todays lunch...</h1>';
        echo '<ul id="cyclelist">';

        $total = count($chosen);
        foreach($chosen as $li){
            $color = $lunch->getColor();
            $last = $lunch->getLast($total);
            $first = $lunch->getfirst($total);

            echo '<li class="'.$color;
            if($last==true){
                echo ' last';
            }
            echo '">'.$li.'</li>';
        }
        echo '</ul>';

        echo '<a href="" class="stop">STOP</a>';
        echo '<a href="./" class="back">< back</a>';
        //echo '<!--a href="" class="start">...carry on</a-->';

    }else{
        echo $form;
    }
    ?>


</div>
</body>
</html>


