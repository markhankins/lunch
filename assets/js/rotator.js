$(document).ready(function() {

    $("#cyclelist li:first-child").addClass("first active"); // Give classes first and active to the first li
    $("#cyclelist li:last-child").addClass("last"); // Give class last to the last li
    $("#cyclelist .active").fadeIn(0);

    var timeSet = 0.2; //Second delay between cycles
    var timeDelay = 100;

    var cycleThru = function (){
        if($(".last").is(":visible")){
            $("#cyclelist .active").removeClass("active");
            $("#cyclelist .first").addClass("active");
            $("#cyclelist li:visible").fadeOut(0, function(){
                $(".active").fadeIn(0);
            });
        }else{
            if($("#cyclelist .active").is(":visible")){
                $("#cyclelist .active").removeClass("active");
                $("#cyclelist li:visible").next("li").addClass("active");
                $(".active").prev("li").fadeOut(0, function(){
                    $(".active").fadeIn(0);
                });
            }
        }
    };

    var cycleInterval = setInterval(function () {
        cycleThru();
    }, timeDelay);

    cycleInterval;


    $('body').on('click','a.stop', function(e) {
        e.preventDefault();
        console.log('stopping');

        var interval_id = window.setInterval("", 9999); // Get a reference to the last interval +1
        for (var i = 1; i < interval_id; i++){
            window.clearInterval(i);
        }

        $(this).addClass('start').removeClass('stop');//switch the button classes
        $(this).text('GO');//switch the button text
    });

    $('body').on('click','a.start', function(e) {

        console.log('start');
        e.preventDefault();

        var interval_id = window.setInterval("", 9999); // Get a reference to the last
        // interval +1
        for (var i = 1; i < interval_id; i++){
            window.clearInterval(i);
        }

        var cycleInterval = setInterval(function () {
         cycleThru();
         }, timeDelay);
        cycleInterval;

        $(this).addClass('stop').removeClass('start');//switch the button classes
        $(this).text('STOP');//switch the button test
        console.log(cycleInterval);
    });

});
