<?php

class Lunch
{
    private $rest;
    private $color;

    //setup whatever we need
    //A list of restaurants and colors for them
    //@todo, concatenate into one array?
    public function __construct(){
        $this->rest = array(
            "Nando's", "Big Philler's", "Fish & Chips", "Greggs", "Tesco", "Toby Carvery", "Walls End", "McDonalds", "Pizza Hut", "Burger King", "KFC", "The Bake", "Spud & Lettuce", "Council Canteen", "CBX Canteen"
        );

        $this->colors = array(
            0=>'red',
            1=>'orange',
            2=>'yellow',
            3=>'green',
            4=>'blue',
            5=>'indigo',
            6=>'violet',
            7=>'white',
        );
    }

    //build the form
    public function getForm($rest){
        $form = '<h1>Get some...</h1><form action="" id="choose" method="post">';
        foreach($this->rest as $choice){
            $name = htmlentities($choice, ENT_QUOTES);
            $form .= '<input type="checkbox" name="'.$name.'" value="'.$name.'" checked /><label for="'.$name.'">'.$name.'</label>';
        }
        $form .= '<button type="submit" name="submit" value="submit">Get Lunch!</button>
</form>';
        return $form;
    }

    //get a random color for each list element
    public function getColor(){
        $count_colors = count($this->colors)-1;
        $i=mt_rand(0,$count_colors);
        return $this->colors[$i];
    }

    public function getFirst(){
        $first = false;
        if($i==0){
            $first = true;
        }
        return $first;
    }

    public function getLast($i){
        $last = false;
        $count = count($this->rest)+1;
        if($i==$count){
            $last = true;
        }
        return $last;
    }
}
?>

